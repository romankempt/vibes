# References

## Work that was performed using `FHI-vibes`

### "Anharmonicity measure for materials"
F. Knoop, T.A.R. Purcell, M. Scheffler, and C. Carbogno, (2020), [published in PRMaterials](https://journals.aps.org/prmaterials/abstract/10.1103/PhysRevMaterials.4.083809), [arXiv:2006.14672](https://arxiv.org/abs/2006.14672)

```
@article{PhysRevMaterials.4.083809,
title = {Anharmonicity measure for materials},
author = {Knoop, Florian and Purcell, Thomas A. R. and Scheffler, Matthias and Carbogno, Christian},
journal = {Phys. Rev. Materials},
volume = {4},
issue = {8},
pages = {083809},
numpages = {12},
year = {2020},
month = {Aug},
publisher = {American Physical Society},
doi = {10.1103/PhysRevMaterials.4.083809},
url = {https://link.aps.org/doi/10.1103/PhysRevMaterials.4.083809}
}
```
